# Jakub Nabywaniec
My Tester's Portfolio
# 📖About me
Software testing is an invariably important part of the software development process. I took my first steps in this direction quite unconsciously
from various applications in which I had the opportunity to find some inaccuracies. I am a person who likes to find holes in everything, and at the same time I focus on details. I decided to become a software tester and my skills will be useful in your team.
# ⛓ Tools
* Confluence, Slack, Jira - Issue tracking and project management tool for developers
* TestRail
* Selenium IDE
* DevTools
* Swagger
* MySQL
* Selenium WebDriver
* IntelliJ IDEA
* Postman
* Gherkin
* Cucumber
* Selenium
* Git
#  ⚙️Tech skills
* Manual Testing
* Java
* Behavior-Driven Development
* (BDD)Waterfall and agile methodologies
* Static Testing
* SQL
* Software Testing Life Cycle (STLC)
* REST APIs
* TestRail
* Git
* Jira
* Selenium
* DevTools
* Test Automation
* Swagger
* MySQL
* Selenium WebDriver
* Software Development Life Cycle (SDLC)
* Test Management
* Testing Tools
* Postman
* Software Testing
* Communication
* Confluence
* Scrum
* Test desing techniques
# 🪪 Certificates
* E.13 Designing local computer networks and administering networks
* E.12 Installation and operation of personal computers and peripheral devices
* Certified exploratory tester - Test IO
* Certified reproduction tester - Test IO
* Certified user story tester - Test IO
# 🖋️Software Development Academy Cours
 I had the pleasure of participating in the "Manual Tester" course organized by the Software Development Academy. Through 133 hours of classes and many hours devoted to independent work, I gained knowledge on the following topics:
* Introduction to Testing
* Test Design Techniques
* Testing Tools (TestRail, Jira, Confluence, Selenium, DevTools, Swagger, Postman)
* Testing applications and websites (SPA, MPA)
* Scrum Methodology
* Programming in Java
* Selenium WebDriver
* Testing in BDD Methodology (Gherkin, Cucumber)
* Basics of Databases - MySQL
* Preparation for ISTQB Exam
* Final Project (Group and Individual)
# 📚 Books
* TypeScript 4. Od początkującego do profesjonalisty
# 👨‍👦‍👦 Groups on Facebook
* QA Testers - job Offers, oferty pracy, jobangedbote
* Testowanie oprogramowania - Materiały | Porady
* Tester oprogramowania - Wsparcie na starcie.
* Playwright.DEV - Polish Community
* Rozmowy rekrutacyjne IT (Programista / Tester / DevOps / Administrator / IT)
* CanTest.IT Remotely - QA Coding Workshops
* Testowanie oprogramowania - Praca, ogłoszenia 
* Testowanie Oprogramowania 
* JavaScript, React, and Node.Js
* Cypress.IO
* Linux PL 
* Unifi Network Official
* Mikrotik Polish Group 
* Programowanie Android 
* Malinowe Pi
* {Początkujący Programista}
* SysOps / DevOps Polska
* React, Next.js Remix - Polska
* Programiści Polska 
* Polska Społeczność AI/ML
* React Polska
* PHPers: After hours 
* Typowi Technicy Informatycy
* Debian Linux / Ubuntu (Polska)
* 
# 💭 Recomended blogs
* testerzy.pl
* 4testers.pl
* Testelka.pl
* @PoSzklanieINaTestowanie
# 👍Webinars
* HR Class 
* Scrum workshops
* IT Interview workshops
* QA 4-day goIT testing marathon
* 7 day goIT coding marathon (html5, css3)
* Google I/O 2023
# 📑My Test Documentation

#### [qa-autocheck-test](https://qa-autocheck-test.netlify.app/?token=d5fcc3783ba50fcac78b5a5ea8e4d69f6fe51ed8368bc618a58a846ad8b03a63&block=nop678917&ssid=64653721fab3d6b4a61cfff1&cookie_id=0c1a4eacd2a44d06a4c438d6b4022a6f&block_id=6430137f6358283e373277e4&leeloo_account_id=64653721fab3d6871d1cffec&utm_source=facebook&utm_medium=cpc&utm_campaign=23854716037160199%7CLV%2B%7C%2BPoland%2B%7C%2BMarathon%2BQA%2B%7C%2BAutoBid%2B%7C%2B23-40%2B%7C%2BNewCreo%7C23854716037150199%7Cmen%7C23854716037170199%7Cad1&fbclid=IwAR0leriYO8NOGfCTdkUmK9Q0SgjbYk09_Sd9F2cv-9Lg6MKM6Hw1FrA3Zn0_aem_th_ARdKP31-L3siP67_LzhFpz3d1nQV_JAQDExjMorB3mk6qogBGG7tMQW7TLNhVutr4Kkpdr6cwGWOg0CbQ_JXjzrS)
# [Specification](https://faq-qa.m.goit.global/pl/?ssid=64653721fab3d6b4a61cfff1&cookie_id=0c1a4eacd2a44d06a4c438d6b4022a6f&block_id=6446d433d3ac67261ac974ec&leeloo_account_id=64653721fab3d6871d1cffec&utm_source=facebook&utm_medium=cpc&utm_campaign=23854716037160199%7CLV%2B%7C%2BPoland%2B%7C%2BMarathon%2BQA%2B%7C%2BAutoBid%2B%7C%2B23-40%2B%7C%2BNewCreo%7C23854716037150199%7Cmen%7C23854716037170199%7Cad1&fbclid=IwAR0leriYO8NOGfCTdkUmK9Q0SgjbYk09_Sd9F2cv-9Lg6MKM6Hw1FrA3Zn0_aem_th_ARdKP31-L3siP67_LzhFpz3d1nQV_JAQDExjMorB3mk6qogBGG7tMQW7TLNhVutr4Kkpdr6cwGWOg0CbQ_JXjzrS)
# Test cases
#### [Changing the languange on the website](https://docs.google.com/spreadsheets/d/1ReFKkAtOp86heketPhx-ucQqLXb4OWLb/edit?usp=sharing&ouid=115986047958479467995&rtpof=true&sd=true)
#### [Withdraw the question](https://docs.google.com/spreadsheets/d/1ReFKkAtOp86heketPhx-ucQqLXb4OWLb/edit#gid=1134308104)
#### [Right arrow in the navigation menu](https://docs.google.com/spreadsheets/d/1ReFKkAtOp86heketPhx-ucQqLXb4OWLb/edit#gid=432236916)
#### ["Check" button](https://docs.google.com/spreadsheets/d/1ReFKkAtOp86heketPhx-ucQqLXb4OWLb/edit#gid=1323724210)
#### ["Reset" button ](https://docs.google.com/spreadsheets/d/1ReFKkAtOp86heketPhx-ucQqLXb4OWLb/edit#gid=650938138)
# Bug reports
#### [ When you press the "EN" button to change the language, nothing happens](https://1drv.ms/i/s!Ags8E2N1JH7PqmTydVZJrMpAQMFL?e=WZhvvZ)
#### [ After presing the "check" button nothing happens](https://1drv.ms/i/s!Ags8E2N1JH7PqmVKmOovGBWjSoQy?e=dATUwv)
#### [Withdral the question](https://1drv.ms/i/s!Ags8E2N1JH7Pqmb9kFP8gePPoPob?e=sRotkW)
